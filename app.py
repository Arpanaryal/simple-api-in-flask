import re
from flask import Flask, request , jsonify

app = Flask(__name__)

cars_list = [
    {
        "id" : 0,
        "brand": "Honda",
        "cc" : 1234,
        "maxspeed" : 200
    },
     {
        "id" : 1,
        "brand": "Audi",
        "cc" : 2222,
        "maxspeed" : 210
    },
     {
        "id" : 2,
        "brand": "Hyundai",
        "cc" : 1456,
        "maxspeed" : 190
    },
     {
        "id" : 3,
        "brand": "Bmw",
        "cc" : 2500,
        "maxspeed" : 250
    },
     {
        "id" : 4,
        "brand": "Mercedes",
        "cc" : 2234,
        "maxspeed" : 250
    }
    
]

@app.route('/cars', methods= ['GET','POST'])
def cars():
    if request.method == 'GET':
        if len (cars_list) > 0:
            return jsonify(cars_list)
        else:
            'Nothing Found', 404

    if request.method =='POST':
        new_brand = request.form['brand']
        new_cc = request.form['cc']
        new_maxspeed = request.form['maxspeed']
        iD = cars_list[-1 ]['id'] + 1

        
        new_obj = {
            'id' : iD,
            'brand' : new_brand,
            'cc': new_cc,
            'maxspeed' : new_maxspeed
        }
        cars_list.append(new_obj)
        return jsonify(cars_list), 201

@app.route('/car/<int:id>', methods =['GET','PUT','DELETE'])      
def single_car(id):
    if request.method == 'GET':
        for car in cars_list:
            if car['id'] == id:
                return jsonify(car)
            pass
           

    if request.method == 'PUT':
        for car in cars_list:
            if car['id'] == id:
                car['brand'] = request.form['brand']
                car['cc'] = request.form['cc']
                car['maxspeed'] = request.form['maxspeed']
                updated_car = {
                    'id' :id,
                    'brand': car['brand'],
                    'cc': car['cc'],
                    'maxspeed': car['maxspeed']
                }
                return jsonify(updated_car)

    if request.method == 'DELETE':
        for index,car in enumerate(cars_list):
            if car['id'] == id:
                cars_list.pop(index)
                return jsonify(cars_list)

if __name__  == '__main__':
    app.run()





               